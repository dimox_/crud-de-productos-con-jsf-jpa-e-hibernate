package com.dimox.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Productos")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_prod;
	
	@Column(unique=true, nullable=false)
	private String nombre_prod;
	
	@Column(length=100)
	private String fabricante_prod;
	
	@Column
	private String detalle_prod;
	
	@Column
	private String descripcion_prod;
	
	@Column(precision = 10, scale = 2, nullable=false) 
	private BigDecimal precio_prod;
	
	@Column
	private int stock_prod;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date create_at = new Date();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date update_at = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, targetEntity = Categoria.class)
	@JoinColumn(name="categoria", nullable=false)
	private Categoria categoria_prod;
	
	// CONSTRUCTORES
	
	public Producto() {}
	
	public Producto(Long id_prod) {
		super();
		this.id_prod = id_prod;
	}
	
	public Producto(String nombre_prod, String fabricante_prod, String detalle_prod, String descripcion_prod,
			BigDecimal precio_prod, int stock_prod, Date create_at, Date update_at, Categoria categoria_prod) {
		super();
		this.nombre_prod = nombre_prod;
		this.fabricante_prod = fabricante_prod;
		this.detalle_prod = detalle_prod;
		this.descripcion_prod = descripcion_prod;
		this.precio_prod = precio_prod;
		this.stock_prod = stock_prod;
		this.create_at = create_at;
		this.update_at = update_at;
		this.categoria_prod = categoria_prod;
	}
	
	public Producto(Long id_prod, String nombre_prod, String fabricante_prod, String detalle_prod,
			String descripcion_prod, BigDecimal precio_prod, int stock_prod, Date create_at, Date update_at,
			Categoria categoria_prod) {
		super();
		this.id_prod = id_prod;
		this.nombre_prod = nombre_prod;
		this.fabricante_prod = fabricante_prod;
		this.detalle_prod = detalle_prod;
		this.descripcion_prod = descripcion_prod;
		this.precio_prod = precio_prod;
		this.stock_prod = stock_prod;
		this.create_at = create_at;
		this.update_at = update_at;
		this.categoria_prod = categoria_prod;
	}

	// GETTERS AND SETTERS
	@PreUpdate
	public void setUpdateAt() {
		this.update_at = new Date();
	}

	public Date getCreate_at() {
		return create_at;
	}

	public Date getUpdate_at() {
		return update_at;
	}
	
	public Long getId_prod() {
		return id_prod;
	}

	public void setId_prod(Long id_prod) {
		this.id_prod = id_prod;
	}

	public String getNombre_prod() {
		return nombre_prod;
	}

	public void setNombre_prod(String nombre_prod) {
		this.nombre_prod = nombre_prod;
	}

	public String getFabricante_prod() {
		return fabricante_prod;
	}

	public void setFabricante_prod(String fabricante_prod) {
		this.fabricante_prod = fabricante_prod;
	}

	public String getDetalle_prod() {
		return detalle_prod;
	}

	public void setDetalle_prod(String detalle_prod) {
		this.detalle_prod = detalle_prod;
	}

	public String getDescripcion_prod() {
		return descripcion_prod;
	}

	public void setDescripcion_prod(String descripcion_prod) {
		this.descripcion_prod = descripcion_prod;
	}

	public BigDecimal getPrecio_prod() {
		return precio_prod;
	}

	public void setPrecio_prod(BigDecimal precio_prod) {
		this.precio_prod = precio_prod;
	}

	public int getStock_prod() {
		return stock_prod;
	}

	public void setStock_prod(int stock_prod) {
		this.stock_prod = stock_prod;
	}

	public Categoria getCategoria_prod() {
		return categoria_prod;
	}

	public void setCategoria_prod(Categoria categoria_prod) {
		this.categoria_prod = categoria_prod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_prod == null) ? 0 : id_prod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id_prod == null) {
			if (other.id_prod != null)
				return false;
		} else if (!id_prod.equals(other.id_prod))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Producto [id_prod=" + id_prod + ", nombre_prod=" + nombre_prod + ", fabricante_prod=" + fabricante_prod
				+ ", detalle_prod=" + detalle_prod + ", descripcion_prod=" + descripcion_prod + ", precio_prod="
				+ precio_prod + ", stock_prod=" + stock_prod + ", create_at=" + create_at + ", update_at=" + update_at
				+ ", categoria_prod=" + categoria_prod.getNombre_cat() + "]";
	}
	
}
