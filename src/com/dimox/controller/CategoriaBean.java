package com.dimox.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

import com.dimox.DAO.CategoriaDao;
import com.dimox.DAO.ICategoriaDao;
import com.dimox.model.Categoria;

@ManagedBean(name = "categoriaBean")
@RequestScoped
public class CategoriaBean {

	ICategoriaDao catDao = null;
	Categoria cat = null;
	
	List<String> nombres_cat = new ArrayList<>();
	List<Categoria> categorias = new ArrayList<>();
	
	private Part file;
	private String folder = "C:\\Spring5\\Apache Tomcat\\apache-tomcat-9.0.13\\webapps\\CRUD-Productos-Hibernate-JPA-JSF\\img\\categorias";

	// GETTERS AND SETTERS
	public List<Categoria> getCategorias() {
		return categorias;
	}
	
	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}
	
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public List<String> getNombres_cat() {
		return this.nombres_cat;
	}
	
	public void setUrlImage(String url) {
		cat.setImagen_cat(url);
	}
	
	// METHODS
	public List<Categoria> obtenerCategorias() {
		catDao = new CategoriaDao();
		categorias=catDao.findAll();
		
		return categorias;
	}

	public List<String> obtenerNombreCategorias() {
		catDao = new CategoriaDao();
		catDao.findAll().forEach(cat -> nombres_cat.add(cat.getNombre_cat()));
		return this.nombres_cat;
	}

	public String crearCategoria() {
		cat = new Categoria();
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sessionMap.put("categoria", cat);
		return "/crear_cat_form.xhtml";
	}

	public String guardarCategoria(Categoria cat) {
		
		catDao = new CategoriaDao();
		
		// Obtenemos el nombre del archivo cargado desde el formulario y su extension
		String nombreImagen = cat.getNombre_cat();
		String ExtensionfileName = FilenameUtils.getExtension(file.getSubmittedFileName());
		String nombreMasExtension = nombreImagen+"."+ExtensionfileName;
		
		// Establecemos la ruta donde se ubica la imagen
		cat.setImagen_cat("img/categorias/"+nombreMasExtension);
		
		// Guardamos la categoria creada
		catDao.save(cat);
		
		saveFile(nombreMasExtension);
		
		
		
		return "/crear_producto_form.xhtml";
	}

	public void saveFile(String nombreMasExtension) {

		if (file != null) {

			try (InputStream input = file.getInputStream()) {
				
				// Crea los directorios de la ruta que no existan
				File ruta = new File(folder);
				ruta.mkdirs();
				
				// Copia el archivo a la ruta establecida 
				Files.copy(input, new File(folder, nombreMasExtension).toPath());
				
				System.out.println("file.getSubmittedFileName = " + file.getSubmittedFileName());
				System.out.println("file.getSize = " + file.getSize());
				System.out.println("nombreMasExtension = " + nombreMasExtension);
				
			} catch (IOException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error! IOException --- ", "Contact admin."));
			}
		}else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No se ha a�adido ning�n archivo"));
			System.out.println("No se ha a�adido ningun archivo!");
		}
	}
	
}
