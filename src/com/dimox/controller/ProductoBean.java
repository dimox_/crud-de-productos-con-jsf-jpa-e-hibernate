package com.dimox.controller;

import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.dimox.DAO.CategoriaDao;
import com.dimox.DAO.ICategoriaDao;
import com.dimox.DAO.IProductoDao;
import com.dimox.DAO.ProductoDao;
import com.dimox.model.Categoria;
import com.dimox.model.Producto;

@ManagedBean (name = "productoBean")
@RequestScoped
@FacesConverter(forClass=Categoria.class)
public class ProductoBean implements Converter {

	IProductoDao prodDAO = null;
	Producto prod = null;
	
	Categoria categ = null;
	ICategoriaDao catDao = null;
//	Long idCat;
	
	public List<Producto> obtenerProductos(){
		prodDAO = new ProductoDao();		
		return prodDAO.findAll();
	}
	
	public List<Producto> obtenerProductosConCategoria(){
		prodDAO = new ProductoDao();		
		return prodDAO.findAll();
	}
	
	public String editarProducto(Long id_prod) {
		prodDAO = new ProductoDao();	
		prod = new Producto();
		prod=prodDAO.findById(id_prod);
		System.out.println(prod);
		
		Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sessionMap.put("producto", prod);
		sessionMap.put("categoria", categ);
		
		return "/editar_form.xhtml";
	}
	
	public String actualizarProducto(Producto prod) {
		
		prodDAO = new ProductoDao();
		
		try {
			prodDAO.update(prod);
		} catch (Exception e) {
			System.out.println("EXCEPTION UPDATE: " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en actualizaci�n: El registro no existe  ", "Contact admin."));
		}
		
		
		return "/index.xhtml";
	}
	
	public String eliminarProducto(Long id_prod) {
		
		prodDAO = new ProductoDao();
		
		try {
			prodDAO.eliminarProducto(id_prod);
		} catch (Exception e) {
			System.out.println("EXCEPTION DELETE: " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error borrado de registro: EL registro no encontrado  ", "Contact admin."));
		}
		
		return "/index.xhtml";
	}
	
	public String crearProducto() {
		
		prod = new Producto();
		
		try {
			Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put("producto", prod);
		} catch (Exception e) {
			System.out.println("EXCEPTION SESSION: " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en sesi�n   ", "Contact admin."));
		}
		
		return "/crear_producto_form.xhtml";
	}
	
	public String guardarProducto(Producto prod) {
		
		prodDAO = new ProductoDao();
		
		try {
			prodDAO.update(prod);
		} catch (Exception e) {
			System.out.println("EXCEPTION PERSIST: " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "El registro que ha intentado registrar ya existe  ", "Contact admin."));
		}
		return "/index.xhtml";
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		 if (arg2 == null || arg2.isEmpty()) {
		        return null;
		    }

		 catDao = new CategoriaDao();
		    try {
		        return catDao.findById(Long.valueOf(arg2));
		    } catch (NumberFormatException e) {
		        throw new ConverterException(new FacesMessage(arg2 + " is not a valid Warehouse ID"), e);
		    }
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
	    if (arg2 == null) {
	        return "";
	    }

	    if (arg2 instanceof Categoria) {
	        return String.valueOf(((Categoria) arg2).getId_cat());
	    } else {
	        throw new ConverterException(new FacesMessage(arg2 + " is not a valid Warehouse"));
	    }
	}
	
}
