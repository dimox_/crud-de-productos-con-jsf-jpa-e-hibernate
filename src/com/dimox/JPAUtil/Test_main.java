package com.dimox.JPAUtil;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.dimox.helpers.Helpers;
import com.dimox.model.Categoria;
import com.dimox.model.Producto;

public class Test_main {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();
		
		List<Producto> proList = new ArrayList<>();
		
		// INSERTAR CATEGORIAS ---------
		Categoria cat1 = new Categoria("Alimentaci�n","/TiendaOnLine-Hibernate-JPA/WebContent/img/alimentacion.png");
		Categoria cat2 = new Categoria("Bebidas","/TiendaOnLine-Hibernate-JPA/WebContent/img/bebidas.png");
		Categoria cat3 = new Categoria("Electr�nica","/TiendaOnLine-Hibernate-JPA/WebContent/img/electronica.png");
		entity.getTransaction().begin();
		entity.persist(cat1);
		entity.persist(cat2);
		entity.persist(cat3);
		entity.getTransaction().commit();
		// -------------------------------
		
		
		// INSERTAR PRODUCTO 1 ---------
		Producto prod = new Producto();
		prod.setNombre_prod("Chocolate");
		prod.setDescripcion_prod("Producto propicio como compolemento de la alimentacion de los m�s peque�os");
		prod.setDetalle_prod("Este es el detalle del producto, que tiene que contener varias lineas");
		prod.setFabricante_prod("Medanasa SL");
		prod.setPrecio_prod(Helpers.getBigTwoDecimals("1234.5678"));
		prod.setStock_prod(150);
		prod.setCategoria_prod(cat1);
		// -------------------------------
		
		System.out.println(prod);
		
		// INSERTAR PRODUCTO 2 ---------
		Producto prod2 = new Producto();
		prod2.setNombre_prod("Horno microondas");
		prod2.setDescripcion_prod("Producto propicio como compolemento de la alimentacion de los m�s peque�os");
		prod2.setDetalle_prod("Este es el detalle del producto, que tiene que contener varias lineas");
		prod2.setFabricante_prod("Medanasa SL");
		prod2.setPrecio_prod(Helpers.getBigTwoDecimals("1234.5678"));
		prod2.setStock_prod(150);
		prod2.setCategoria_prod(cat2);
		// -------------------------------
		
		System.out.println(prod2);
		
		// PERSISTIR PRODUCTOS
		entity.getTransaction().begin();
		entity.persist(prod);
		entity.persist(prod2);
		entity.getTransaction().commit();		
		// -------------------------------
		
		
		System.out.println(prod.getNombre_prod());
		System.out.println(prod.getCategoria_prod().getNombre_cat());
		
		
//		System.out.println("hola " + cat2.getNombre_cat() + "\n");
//		proList = cat2.getProductoList();
//		for (Producto producto : proList) {
//			System.out.println("1 - " + producto);
//		}
//		
		
//		ICategoriaDao catDao = null;
//		Categoria cat = null;
//		
//		List<String> nombres_cat = new ArrayList<>();
//		
//		catDao = new CategoriaDao();
//		catDao.findAll().forEach(cate -> nombres_cat.add(cate.getNombre_cat()));		
//		
//		nombres_cat.forEach(c -> System.out.println(c));
		
		
//		prod = entity.find(Producto.class, 1);
//		System.out.println(prod);
//		
//		prod.setId_prod(1);
//		prod.setCategoria_prod("Casa");
//		prod.setNombre("Chocolate");
//		prod.setDescripcion_prod("Producto propicio como compolemento de la alimentacion de los m�s peque�os");
//		prod.setDetalle_prod("Este es el detalle del producto, que tiene que contener varias lineas");
//		prod.setFabricante_prod("Medanasa SL");
//		prod.setPrecio_prod(Helpers.getBigTwoDecimals("1234.5678"));
//		prod.setStock_prod(150);
//		
//		entity.getTransaction().begin();
//		entity.merge(prod);
//		entity.getTransaction().commit();
//		
//		prod = entity.find(Producto.class, 1);
//		System.out.println(prod);
//		
//		entity.getTransaction().begin();
//		entity.remove(prod);
//		entity.getTransaction().commit();
//		
	}
	
	
}
