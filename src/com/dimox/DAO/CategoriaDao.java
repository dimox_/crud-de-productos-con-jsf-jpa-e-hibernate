package com.dimox.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.dimox.JPAUtil.JPAUtil;
import com.dimox.model.Categoria;

public class CategoriaDao implements ICategoriaDao {

	EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	@Override
	public void save(Categoria categoria) {
		entity.getTransaction().begin();
		entity.persist(categoria);
		entity.getTransaction().commit();
	}

	@Override
	public void update(Categoria categoria) {
		entity.getTransaction().begin();
		entity.merge(categoria);
		entity.getTransaction().commit();
	}

	@Override
	public Categoria findById(Long id_cat) {
		Categoria cat = new Categoria();
		cat = entity.find(Categoria.class, id_cat);
		return cat;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> findAll() {
		List<Categoria> catList = new ArrayList<>();
		Query query = entity.createQuery("SELECT c FROM Categoria c");
		catList = query.getResultList();
		return catList;
	}

	@Override
	public void eliminarCategoria(Long id_cat) {
		entity.getTransaction().begin();
		entity.remove(id_cat);
		entity.getTransaction().commit();
	}
	
	
}
