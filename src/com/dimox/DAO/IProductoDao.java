package com.dimox.DAO;

import java.util.List;

import com.dimox.model.Producto;

public interface IProductoDao {

	public void save(Producto porducto);
	public void update(Producto porducto);
	public Producto findById(Long id_prod);
	public List<Producto> findAll();
	public void eliminarProducto(Long id_prod);
	
}
