package com.dimox.DAO;

import java.util.List;

import com.dimox.model.Categoria;

public interface ICategoriaDao {
	
	public void save(Categoria categoria);
	public void update(Categoria categoria);
	public Categoria findById(Long id_cat);
	public List<Categoria> findAll();
	public void eliminarCategoria(Long id_cat);
}
