package com.dimox.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.dimox.JPAUtil.JPAUtil;
import com.dimox.model.Producto;

public class ProductoDao implements IProductoDao {

	EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	// Crear producto
	@Override
	public void save(Producto producto) {

		try {
			entity.getTransaction().begin();
			entity.persist(producto);
			entity.getTransaction().commit();

		} catch (Exception e) {
			entity.getTransaction().rollback();
			System.out.println("EXCEPTION PERSIST: " + e.getMessage());
			throw e;
		} finally {
			// JPAUtil.shutdown();
		}

	}

	// Editar producto
	@Override
	public void update(Producto porducto) {

		try {
			entity.getTransaction().begin();
			entity.merge(porducto);
			entity.getTransaction().commit();
		} catch (Exception e) {
			entity.getTransaction().rollback();
			throw e;
		} finally {
			// JPAUtil.shutdown();
		}

	}

	// Buscar un producto por medio de su id
	@Override
	public Producto findById(Long id_prod) {
		Producto producto = new Producto();
		producto = entity.find(Producto.class, id_prod);
		// JPAUtil.shutdown();
		return producto;
	}

	// Traer todos los productos existentes
	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> findAll() {
		List<Producto> listProductos = new ArrayList<>();
		
		Query query = entity.createQuery("SELECT c FROM Producto c");
		listProductos = query.getResultList();
		
		return listProductos;
	}

	// Eliminar un producto
	@Override
	public void eliminarProducto(Long id_prod) {
		Producto producto = new Producto();

		try {
			producto = entity.find(Producto.class, id_prod);
			entity.getTransaction().begin();
			entity.remove(producto);
			entity.getTransaction().commit();
		} catch (Exception e) {
			entity.getTransaction().rollback();
			throw e;
		} finally {
			// JPAUtil.shutdown();
		}
	}
}
